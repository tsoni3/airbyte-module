# Airbyte deployed on GCP VM

## Notes
- Referencing the project should be done using `data.google_project.working_project.project_id` to make sure test/prod are referenced based on branch name.

## ToDo
- [ ] VM deployed
- [ ] AirByte install script run
- [ ] Octavia config pushed
- [ ] Monitoring added to module
- [ ] Static IP / FW rules written to module
- [ ] IAC-specific code added to module