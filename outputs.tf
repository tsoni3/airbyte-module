# output "ipaddress" {
#   value       = google_compute_instance.default.network_interface.0.access_config.0.nat_ip
#   description = "external ip accessible"
# }

output "username" {
  value       = "${var.nro}-admin"
  description = "external ip accessible"
}
output "password" {
  value       = random_password.pass.result
  description = "external ip accessible"
}

output "ipaddress" {
  value       = google_compute_address.static.address
  description = "external ip accessible"
}

output "lb_ipaddress" {
  value       = google_compute_global_address.static.address
  description = "Loadbalancer IP address."
}

output "airbyte_dns" {
  value       = google_dns_record_set.airbyte_dns_record.name
  description = "Airbyte DNS"
}

output "grafana_dns" {
  value       = google_dns_record_set.grafana_dns_record.name
  description = "Grafana DNS"
}

output "prometheus_dns" {
  value       = google_dns_record_set.prometheus_dns_record.name
  description = "Prometheus DNS"
}
